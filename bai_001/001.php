<?php 
	// Đầu vào: Mảng chứa các phần tử có kiểu số, độ dài bất kỳ
	$mang = array(7, 5, 9,1.34, 6,12, 10, -1.23, -4, -5.25);
 	
 	// Sử dụng hàm count() đếm tổng số phần tử trong mảng.
	$total = count($mang);

	// Tạo biến $max và $min lưu phần tử đầu tiên $mang[0]
	// Và xem nó như là phần tử lớn nhất và đem nó đi so sánh với các phần tử còn lại 
	$max = $mang[0];
	$min = $mang[0];

	// Sử dụng vòng lặp for để so sánh.
	for ($i=0; $i < $total; $i++) { 
		// Kiểm tra phần tử nào lớn hơn $max lập tức gán giá trị phần tử đó cho $max
		if ($max < $mang[$i]) {
			$max = $mang[$i];
		}else {
			// Kiểm tra phần tử nào nhỏ hơn $min lập tức gán giá trị phần tử đó cho $min
			if ($min > $mang[$i]) {
				$min = $mang[$i];
			}
		}
	}

	for ($y=0; $y < $total; $y++) { 
		echo "$mang[$y] ";
	}
	echo "<br> Tổng các số đã nhập vào là: $total <br>";
	echo "Số lớn nhất trong mảng là: $max <br> Số nhỏ nhất trong mảng là: $min";