<?php
	// Tạo $mang chứa các phần tử trùng lặp
	// Có các kiểu dữ liệu (kiểu số(INT), kiểu kiểu số thực(float)), kiểu Kiểu Chuỗi(string)), ...)
	$mang = array(1, 2, 3, 4, 5, 4, 2, 1, 12.54, 1.2, 3.2, 23.1, 12.54, "a", "b", "c", "d", "b", "d", "a"); 

	// Sử dụng hàm count() đếm tổng số phần tử trong mảng
	$tong = count($mang); 

	// Sử dụng 2 vòng lặp for lồng nhau để tiến hành loại bỏ phần tử trùng lặp
	// khởi tạo vòng lặp for thứ nhất
	for ($i = 0; $i < $tong; $i++) { 

		// Tạo một biến có tên là $pttl
	    $pttl = $mang[$i];

	    // khởi tạo vòng lặp for thứ hai
    	for ($k = 0; $k < $tong; $k++) { 

    		// kiểm tra $k không bằng(khác) với $i
    		if ($k != $i) { 
    			// Sẩy ra trường hợp $pttl bằng $mang[$k]
     			if ($pttl == $mang[$k]) { 

     				// truyền luôn giá trị của $mang[$k] bằng rỗng.
     				$mang[$k] = null; 
     			} 
    		} 
    	} 
	} 

	// Sử dụng vòng lặp for in ra giá trị của mảng khi các phần tử trùng lặp đã được truyền giá trị bằng null hoặc loại bỏ.
	for ($i = 0; $i < $tong; $i++) { 
	    echo "$mang[$i] ";
	    // var_dump($mang[$i]);
	} 